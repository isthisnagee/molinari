use cairo::Context;
use geom::RectanglePoints;

pub trait Draw {
    fn draw(&self, context: &Context, color: &Color, fill: bool);
}

impl Draw for RectanglePoints {
    fn draw(&self, context: &Context, color: &Color, fill: bool) {
        let mut points = self.points.iter();
        let first_pt = points.nth(0).unwrap();

        context.move_to(first_pt.x, first_pt.y);
        for pt in points {
            context.line_to(pt.x, pt.y);
        }
        context.line_to(first_pt.x, first_pt.y);

        context.set_source_rgb(color.r, color.g, color.b);
        if fill {
            context.fill();
        } else {
            context.stroke();
        }
    }
}

pub struct Color {
    r: f64,
    g: f64,
    b: f64,
}

impl Color {
    pub fn new(r: f64, g: f64, b: f64) -> Color {
        Color {
            r: r / 256.,
            g: g / 256.,
            b: b / 256.,
        }
    }
}
