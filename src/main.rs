extern crate cairo;
extern crate num;
extern crate rand;
extern crate time;

use std::fs::File;

use cairo::{Context, Format, ImageSurface};
mod geom;
mod draw;

fn main() {
    let red = || draw::Color::new(160., 40., 32.);
    let black = || draw::Color::new(19., 19., 17.);
    let white = || draw::Color::new(232., 211., 190.);
    let boxes: Vec<(geom::RectanglePoints, draw::Color)> = vec![
        (geom::RectanglePoints::unit().scale(205., 220.), black()),
        (geom::RectanglePoints::unit().scale(205., 75.).translate(0., 220.), white()),
        (geom::RectanglePoints::unit().scale(131., 220.).translate(0., 295.), red()),
        (geom::RectanglePoints::unit().scale(74., 220.).translate(131., 295.), black()),
        (geom::RectanglePoints::unit().scale(75., 515.).translate(205., 0.), red()),
        (geom::RectanglePoints::unit().scale(80., 515.).translate(280., 0.), black()),
        (geom::RectanglePoints::unit().scale(65., 515.).translate(360., 0.), white()),
        (geom::RectanglePoints::unit().scale(80., 257.).translate(425., 0.), red()),
        (geom::RectanglePoints::unit().scale(80., 257.).translate(425., 257.), black()),
    ];
    let now = time::now();
    let grid_size = 600;
    let surface = ImageSurface::create(Format::ARgb32, grid_size as i32, grid_size as i32)
        .expect("could not create image");
    let context = Context::new(&surface);

    for (bx, color) in boxes.into_iter() {
        draw::Draw::draw(&bx.skew(), &context, &color, true);
    }

    let s_now = now.strftime("%d-%m-%y-%I-%M-%S").unwrap();
    std::fs::create_dir_all("output").expect("couldn't create directory `output`");
    let mut file = File::create(format!("output/{}.png", s_now)).expect("Couldn’t create file.");
    surface
        .write_to_png(&mut file)
        .expect("Couldn’t write to png");
    println!("output/{}.png", s_now);
}
