use rand::{thread_rng, Rng};
pub struct Point {
    pub x: f64,
    pub y: f64,
}

pub struct RectanglePoints {
    pub points: Vec<Point>,
}

impl Point {
    pub fn new(x: f64, y: f64) -> Point {
        Point { x: x, y: y }
    }

    pub fn translate(&self, x: f64, y: f64) -> Point {
        Point {
            x: self.x + x,
            y: self.y + y,
        }
    }

    pub fn scale(&self, x: f64, y: f64) -> Point {
        Point {
            x: self.x * x,
            y: self.y * y,
        }
    }
}

impl RectanglePoints {
    pub fn unit() -> RectanglePoints {
        RectanglePoints {
            points: vec![
                Point::new(0., 0.),
                Point::new(1., 0.),
                Point::new(1., 1.),
                Point::new(0., 1.),
            ],
        }
    }

    pub fn translate(self, x: f64, y: f64) -> RectanglePoints {
        RectanglePoints {
            points: self.points.iter().map(|p| p.translate(x, y)).collect(),
        }
    }

    pub fn scale(self, x: f64, y: f64) -> RectanglePoints {
        RectanglePoints {
            points: self.points.iter().map(|p| p.scale(x, y)).collect(),
        }
    }

    pub fn skew(self) -> RectanglePoints {
        let mut rng = thread_rng();
        let mut rng1 = thread_rng();
        let mut rng2 = thread_rng();
        let mut pm = |x: f64| {
            if rng2.gen_range(0., 1.) < 0.2 {
                x
            } else {
                -x
            }
        };
        let mut x = || rng.gen_range(-10., 10.);
        let mut y = || rng1.gen_range(-10., 10.);

        RectanglePoints {
            points: self.points
                .iter()
                .map(|p| Point {
                    x: p.x + pm(x()),
                    y: p.y + pm(y()),
                })
                .collect(),
        }
    }
}
