# Molinari

by [@isthisnagee](https://twitter.com/isthisnagee)

Got this idea while I was at the Art Gallery of Ontario.

## Plan:

- Reproduce the image
- Warp the image (this is the fun part)
    - I might try warping it so shapes are simliar to [beta =
        10^-4](https://people.mpi-inf.mpg.de/~chen/papers/aaap.pdf)

# Latest

## No Warp
![molinari](current-unwarped-molinari.png)

## Warp
![warped-molinari](current-molinari.png)

## Process

> the background color was transparent so you'll see previous images leak into
> newer ones. Sorry!

![molinari-gif](progress.gif)
